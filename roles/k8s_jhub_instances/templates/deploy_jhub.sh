#!/usr/bin/bash

# Check if changes have been commited to Git
if [ "$(git status --porcelain | wc -l)" -ne 0 ]; then
    read -p "Uncommited changes found. Continue? [y/N] "
    
    if [ "$REPLY" != "y" ] && [ "$REPLY" != "Y" ]; then
	echo Exiting.
	exit
    fi
else
    echo All changes are commited, continuing.
fi


# Make sure we are talking to the desired cluster
kubectl get nodes
echo
read -p "Check nodes: kubectl context ok? [y/N] "
if [ "$(echo $REPLY|tr y Y)" != "Y" ];
then
    echo Aborting.
    exit
fi

# Deploy pvc's
kustomize build . | kubectl apply -f -
# Deploy API for Moodle assignments
kustomize build moodle-api | kubectl apply -f -

RELEASE={{ item.name }}
NAMESPACE={{ item.name }}
{% if item.jhub_chart_version is defined %}
VERSION={{ item.jhub_chart_version }}
{% else %}
VERSION={{ jhub_chart_version }}
{% endif %}

helm upgrade --cleanup-on-fail \
  --install $RELEASE jupyterhub/jupyterhub \
  --namespace $NAMESPACE \
  --create-namespace \
  --version=$VERSION \
  --values values.yml
