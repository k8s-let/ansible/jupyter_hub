#!/usr/bin/bash

# Make sure we are talking to the desired cluster
kubectl get nodes
echo
read -p "Check nodes: kubectl context ok? [y/N] "
if [ "$(echo $REPLY|tr y Y)" != "Y" ];
then
    echo Aborting.
    exit
fi

echo
cat INFO.yml
echo
read -p "Do you really want to delete this hub? "
if [ "$(echo $REPLY|tr y Y)" != "Y" ];
then
    echo Aborting.
    exit
fi

RELEASE={{ item.name }}
NAMESPACE={{ item.name }}
VERSION={{ jhub_chart_version }}

helm delete $RELEASE --namespace $NAMESPACE

# Remove API for Moodle assignments
kustomize build moodle-api | kubectl delete -f -

# Remove pvc's
kustomize build . | kubectl delete -f -

echo "You should remove the instance from host_vars/localhost.yml now."
echo "Copy it to host_vars/archived_hubs.yml, in case it has to be restored."
