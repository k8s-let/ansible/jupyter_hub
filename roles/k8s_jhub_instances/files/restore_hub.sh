#!/usr/bin/bash
#
# Restore JupyterHub user directories
#
# Copy user directories from an archived persistent volume directory to a reenabled JupyterHub

if [ "$#" != "1" ]; then
    echo "Moodle course ID is missing"
    exit 1
fi

MOODLE_COURSE_ID=$1

#
# Make sure we are talking to the desired cluster
#
kubectl get nodes
echo
read -p "Check nodes: kubectl context ok? [y/N] "
if [ "$(echo $REPLY|tr y Y)" != "Y" ];
then
    echo Aborting.
    exit
fi

echo "Searching archived hubs..."

#
# Count matching archived directories.
# Do not continue if zero or more than 1 archive exist
#
ARCHIVED_HUBS_COUNT=$(kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "ls -d /mnt/archived*jhub-let*jhub-let*/" | grep "\-$MOODLE_COURSE_ID\-" | wc -l)

if [ $ARCHIVED_HUBS_COUNT == "0" ]; then
    echo
    echo "No archives found"
    exit 1
fi
if [ $ARCHIVED_HUBS_COUNT != "1" ]; then
    echo
    echo "More than one archive found, cannot continue."
    echo "Clean up archives first or restore manually."
    echo
    kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "ls -d /mnt/archived*jhub-let*jhub-let*/" | grep "\-$MOODLE_COURSE_ID\-"
    exit 1
fi

#
# Inform about restored directories
#
RESTORED_HUBS_COUNT=$(kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "ls -d /mnt/restored_archived*jhub-let*jhub-let*/" | grep "\-$MOODLE_COURSE_ID\-" | wc -l)

if [ $RESTORED_HUBS_COUNT != "0" ]; then
    echo
    echo "Found a directory used for restore earlier. This is for information"
    echo "only and not considered an issue."
    echo
    kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "ls -d /mnt/restored_archived*jhub-let*jhub-let*/" | grep "\-$MOODLE_COURSE_ID\-"
    echo
fi

#
# All fine, now show contents of source directory
#
export SOURCE=$(kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "cd /mnt; ls --color=never -d archived*jhub-let*jhub-let*/" | grep $MOODLE_COURSE_ID | tr -d '[:space:]')

echo "Source found and unique. Here is the content:"
echo
kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d ' ' -f 1) -n local-system -- /usr/local/bin/bash -c "ls /mnt/${SOURCE}"

#
# Count target directories
#
echo
NEW_HUBS_COUNT=$(kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "ls -d /mnt/jhub-let-*jhub-let*/" | grep $MOODLE_COURSE_ID | wc -l)
if [ $(echo $NEW_HUBS_COUNT) == "0" ]; then
    echo
    echo "No active hub directory found"
    exit 1
fi
if [ $(echo $NEW_HUBS_COUNT) != "1" ]; then
    echo
    echo "More than one active hub directory found, cannot continue."
    echo "This is STRANGE, investigate!"
    exit 1
fi

#
# Contents of target directory
#
export TARGET=$(kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "cd /mnt; ls --color=never -d jhub-let-*jhub-let*" | grep $MOODLE_COURSE_ID | tr -d '[:space:]')

echo "Contents of target directory:"
echo
kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "ls /mnt/${TARGET}" 2>/dev/null

#
# Size check
#

echo
echo "Restores may take some time, ca. 2 minutes per GB."
read -p "Do you want to check the size of the restore [Y/n]? "
if [ $(echo ${REPLY:-Y} | tr yn YN) != "N" ]; then
    echo "Checking size (takes about 5-10s per GB, be patient)"
    echo
    kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d ' ' -f 1) -n local-system -- /usr/local/bin/bash -c "du -hs /mnt/${SOURCE}"
fi

#
# Perform restore operation
#
echo
read -p "Do you want to continue and copy the files [y/N]? "
if [ $(echo ${REPLY:-N} | tr yn YN) != "Y" ]; then
    echo Operation aborted.
    exit 1
fi

kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "cd /mnt/${SOURCE} && find . | cpio -pdm /mnt/${TARGET}"

#
# Rename source directory to avoid confusion if we would try to restore again
#
kubectl exec --stdin --tty $(kubectl get pods -n local-system | grep admin-pod| cut -d " " -f 1) -n local-system -- /usr/local/bin/bash -c "mv /mnt/${SOURCE} /mnt/restored_${SOURCE}"


echo "File restore completed"
