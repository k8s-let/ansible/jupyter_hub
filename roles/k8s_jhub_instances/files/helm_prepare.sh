#!/usr/bin/bash
#
# Add the helm repository for Jupyter Hub to local environment
#
helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
helm repo update
