k8s_jhub_instances
=========

Reads a list of simplified configuration variables, creates a directory `deployments`, and writes the files
- `deploy_jhub.sh`, `remove_jhub.sh`: main (de)installation scripts
- `INFO.yml`: description extracted from the instance item
- `values.yml`: values to customize the JupyterHub [helm chart](https://jupyterhub.github.io/helm-chart/)
- `kustomization.yml`: builds all configurations except the Helm stuff
- `namespace.yml`: namespace definition for Kubernetes
- `pvc.yml`: persistent volume claim for the user home directories
- `moodle-api`: kustomization directory for the Moodle API for the assignment plugin

to subdirectories named after the Jupyter Hub instances.

Role Variables
--------------

The JupiterHub instance configuration is named `hub_instances` and has the form of a list:
```yaml
hub_instances:
  - name: jhub-cltst-test
    proxyToken: <see below>
    hubSecret: <see below>
    ngshareToken: <see below>
    defaultUrl: "/lab"  #  Required for Helm chart <2.0
    lti:
      key: <see below>
      secret: <see below>
    homeSize: 100Mi
    homeStorageClass: nfs-idsd
    homeGlobalSize: 1000Mi
    memory:  # optional
      guarantee: '256M'
      limit: '1024M'
    cpu:     # optional
	  guarantee: '1'
	  limit: '2'
    imagePullPolicy: Always  # optional
    images:  # repeat as needed
      - display_name: "Full Scipy environment"
        description: "Scipy Python with ETHZ extensions"
        image: registry.ethz.ch/k8s-let/notebooks/jh-notebook-scipy-ethz
        tag: 1.0.0
```

### Secrets
While it is not a requirement for all secrets, it is advised to create them all with a command like
```
openssl rand -hex 32
```

to have a common approach for all secrets.

### Other Variables
See also the `defaults.yml` for a complete list of parameters!

- `defaultUrl`: either `/lab` for the new user interface or `/` for the classic UI
- `lti.key`, `lti.secrets`: must correspond to the values entered in the Moodle *External Tool*
- `homeSize`, `homeGlobalSize`: disk sizes for users and for the whole instance.
- `homeStorageClass`: storage class pool where the home directories are created. Note that the storage class `nfs-idsd` (NFS) does not enforces the limitations, and that the underlying size may be increased without interruption (in contrast to the other classes available which do not allow resizing at all)
- `memory.guarantee`: a new user pod will be spawned only if a node has this amount of free memory available
- `memory.limit`: hard memory limit for the pod
- `cpu.guarantee`: similar for CPU allocation
- `cpu.limit`: hard CPU usage limit for the pod
- `images`: list of images available on this JupyterHub

Memory and CPU limits are optional and only needed if the standard values don't match the course requirements.

## Installation
Install the Ansible collection:

```
ansible-galaxy collection install -f git+https://gitlab.ethz.ch/k8s-let/ansible/jupyter_hub.git
```

The option `-f` ensures an upgrade of an already installed collection.

In the directory where the Ansible variables and the deployments will be places, create a simple playbook `create_config_files.yml` like this:

```yaml
---
- hosts: localhost
  roles:
    - let.jupyter_hub.k8s_jhub_instances
```

## Deployment
 1. Create/edit `host_vars/localhost.yml`
 1. Run `ansible-playbook create_config_files.yml`
 1. go to `deployments/<jhub directory>`
 1. make sure you talk to the correct cluster (will be checked)
 1. run `./deploy_hub.sh`
