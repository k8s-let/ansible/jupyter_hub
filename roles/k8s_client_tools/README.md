Ansible Role - k8s_client_tools
=========

Installs client tools for Kubernetes:

 - kubectl
 - helm
 - kubeseal
 - kustomize
 - ArgoCD CLI
 - Rancher CLI

If possible, installation packages from OS repositories are used.
But most of the tools are available as tarballs only. Currently,
the latest stable version is installed.

Tested on
 - (RH)EL 8
 - Ubuntu 20.04
 - Debian 10 'buster'

Requirements
-----------

Root access to the local machine is required. If run as unpriviledged
user, `sudo` will be used to install the OS packages. Or run it
directly as root (see below).

To use collections, you should upgrade Ansible to a version 2.10+. If
your distribution does not support a current version, use pip (pip3):

```
# Remove system supplied Ansible first, i.e.
# > apt remove ansible
# Then:
pip3 install ansible
```

Role Variables
--------------

 - `k8s_tools_install_mode_root`: if `true`, all Python libraries and binaries
   are installed systemwide. Binaries are installed in `/usr/local/bin`.

   If `false` (default), binaries go to `$HOME/bin` of the user running the playbook,
   and Python libraries are installed for this user only.

 - `k8s_sealedsecrets_controller_name`, `k8s_sealedsecrets_controller_namespace`, `k8s_sealedsecrets_format`: defaults for `sealed-secrets`, written to environment
  variables.

 - `k8s_sealedsecrets_profile`: if installing in user mode, this is the file used
   to preset the settings for `sealed-secrets`. Defaults to `.bashrc`.

 - `k8s_tools`: list of tools installed. Each tool is handled in a separate
   `install_<toolname>.yml`. Remove from the list, if a tool should not be
   installed.

 - `k8s_tools_github_token`: The Github API enforces rate limits which may
   block access during CI/CD testing; for an ordinary installation run,
   no token should be necessary.
   To run the Molecule tests, set the environment variable `GITHUB_API_READ_TOKEN',
   it will be read during the prepare stage.

Example Playbook
----------------

Install the collection first:

```
ansible-galaxay collection install git+https://gitlab.ethz.ch/k8s-let/ansible/jupyter_hub
```

Declare the collection and include the role:

```
- hosts: localhost
  collections:
    - let.jupyter_hub
  vars:
    k8s_tools_install_mode_root: true
  tasks:
    - name: Install Kubernetes client tools
      include_role:
      name: k8s_client_tools
```

License
-------

MIT
