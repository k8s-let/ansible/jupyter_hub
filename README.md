[![pipeline status](https://gitlab.ethz.ch/k8s-let/ansible/k8s_client_tools/badges/master/pipeline.svg)](https://gitlab.ethz.ch/k8s-let/ansible/k8s_client_tools/-/commits/master)
# Ansible Collection - let.jupyter_hub

Configuration roles for Kubernetes and the Jupyter Hub deployment.

 - `k8s_client_tools`: install Kubernetes management tools on a management workstation
 - `k8s_jhub_instances`: create and maintain configuration files for JupyterHub instances
